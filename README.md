# Description

The Application is intended to relay Emails to Telegram Chats (both private and groups).

The Application binds to a local port with SMTP-server like service and connects to Telegram API
using Long Poll method.

Emails sent to `<chat id>@<domain>` where `<chat id>` is the recipient chat ID and `<domain>` is
the configured `@domain` in the application are parsed and relayed to the chat.
The attached files are sent to the chat as `Response` messages (unreliable, has problems with plaintext files, needs to be fixed).

# Start arguments

The Application has internal arguments help which could be printed using `application -h`.

Help:
```shell
./service -h
Usage of ./service:
  -config string
        Defines a configuration file path. (default "config.json")
  -service string
        Controls the service status.
```

# Configuration

The Configuration file contains 3 parameters which are described below:
```json
{
    "smtpBind": ":8080", // SMTP host:port bind string.
    "emailDomain": "example.com", // Domain to use for email@domain in SMTP exchange.
    "botAPIKey": "" // Telegram Bot API key.
}
```
