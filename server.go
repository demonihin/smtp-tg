package smtptg

import (
	"context"

	"github.com/alash3al/go-smtpsrv/v3"
	"go.uber.org/zap"
	"gopkg.in/tucnak/telebot.v3"
)

// AuthFunc defines a method to authenticate SMTP clients.
type AuthFunc func(username, password string) error

// Instance defines an instance of the Server.
type Instance struct {
	logger *zap.Logger

	ctx context.Context

	bot        *telebot.Bot
	smtpConfig *smtpsrv.ServerConfig

	domain string
}

// NewContext creates a new Server Instance.
func NewContext(ctx context.Context, logger *zap.Logger,
	smtpBind string, apiKey string, domain string,
	authFunc AuthFunc) (*Instance, error) {
	// Configure Bot.
	bot, err := initBotContext(ctx, logger, apiKey, domain)
	if err != nil {
		return nil, err
	}

	// Configure SMTP.
	smtpConfig := initSMTPServerConfig(
		logger,
		smtpBind,
		smtpsrv.AuthFunc(authFunc),
		(&botSender{bot: bot}).sendBot, domain)

	return &Instance{
		ctx:        ctx,
		bot:        bot,
		logger:     logger,
		domain:     domain,
		smtpConfig: smtpConfig,
	}, nil
}

// ListenAndServe starts serving SMTP.
func (s *Instance) ListenAndServe() error {
	s.logger.Info("Starting Server", zap.String("email domain", s.domain))
	defer func() {
		s.logger.Info("Stopped Server")

		_ = s.logger.Sync()
	}()

	// Start TG Bot.
	go func() {
		s.logger.Info("Starting TG Bot Handler")
		defer s.logger.Info("Stopped TG Bot Handler")

		s.bot.Start()
	}()
	go func() {
		<-s.ctx.Done()

		s.logger.Info("Stopping TG Bot Handler")

		s.bot.Stop()
	}()

	// Start SMTP Server.
	return smtpsrv.ListenAndServe(s.smtpConfig)
}
