package config

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
)

// Configuration contains the configuration for the service.
type Configuration struct {
	// SMTPBind defines an IP and port to bind the SMTP gateway to.
	SMTPBind string `json:"smtpBind,omitempty" yaml:"smtpBind,omitempty"`
	// BotAPIKey defines an API key for the Bot.
	BotAPIKey string `json:"botAPIKey,omitempty" yaml:"botAPIKey,omitempty"`
	// EmailDomain defines a domain to which the emails must be sent to be forwarded to TG.
	EmailDomain string `json:"emailDomain,omitempty" yaml:"emailDomain,omitempty"`
}

// ParseJSON parses configuration.
func ParseJSON(b []byte) (*Configuration, error) {
	var cfg Configuration

	if err := json.Unmarshal(b, &cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}

// ReadParseJSON reads from input and parses configuration.
func ReadParseJSON(r io.Reader) (*Configuration, error) {
	bts, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	return ParseJSON(bts)
}

// Load opens a configuration file and loads the configuration from it.
func Load(filename string) (*Configuration, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return ReadParseJSON(f)
}
