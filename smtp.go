package smtptg

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/alash3al/go-smtpsrv/v3"
	"go.uber.org/zap"
	"gopkg.in/tucnak/telebot.v3"
)

// MaxMessageBytes - max message attachment size in Telegram.
const MaxMessageBytes = 20 * 1024 * 1024

// ReadWriteTimeout defines a sane read timeout for SMTP Server.
const ReadWriteTimeout = 30 * time.Second

var ErrIncorrectRecipient = errors.New("incorrect recipient")

type SendFunc func(chatID string, msg string, attachments ...interface{}) error

func initSMTPServerConfig(logger *zap.Logger, bind string, auther smtpsrv.AuthFunc, sender SendFunc, domain string) *smtpsrv.ServerConfig {
	cfg := &smtpsrv.ServerConfig{
		ListenAddr:      bind,
		BannerDomain:    domain,
		MaxMessageBytes: MaxMessageBytes,
		Auther:          auther,
		ReadTimeout:     ReadWriteTimeout,
		WriteTimeout:    ReadWriteTimeout,
		Handler: func(c *smtpsrv.Context) (err error) {
			msgLogger := logger.With(
				zap.String("from", c.From().Address),
				zap.String("to", c.To().Address))

			msgLogger.Info("Got email")

			// Log Errors.
			defer func() {
				if err != nil {
					msgLogger.Error("Can not process email", zap.Error(err))
				}
			}()

			// Check Domain.
			sp := strings.Split(c.To().Address, "@")
			if len(sp) != 2 { // 2 is username@domain.
				return fmt.Errorf("%w: can not parse RCPT TO to User and Domain", ErrIncorrectRecipient)
			}

			if strings.ToLower(sp[1]) != domain {
				return fmt.Errorf("%w: RCPT Domain %q is not the configured domain %q", ErrIncorrectRecipient, sp[1], domain)
			}

			// Parse message.
			msg, err := c.Parse()
			if err != nil {
				return err
			}

			// Create Attachments.
			var tgAttachments = make([]interface{}, 0, len(msg.Attachments))

			for _, att := range msg.Attachments {
				file := telebot.FromReader(att.Data)

				tgAttachments = append(tgAttachments, &telebot.Document{
					File:     file,
					FileName: att.Filename,
				})
			}

			// Relay to TG.
			if err := sender(sp[0], fmt.Sprintf("From: %s\nTo: %s\nSubject: %s\n\n%s",
				c.From().Address,
				c.To().Address,
				msg.Subject,
				msg.TextBody),
				tgAttachments...); err != nil {
				return err
			}

			msgLogger.Info("Relayed message to TG",
				zap.String("subject", msg.Subject))

			return nil
		},
	}

	return cfg
}
