package smtptg

import (
	"context"
	"fmt"
	"time"

	"go.uber.org/zap"
	"gopkg.in/tucnak/telebot.v3"
)

func initBotContext(ctx context.Context, logger *zap.Logger, apiKey, mailDomain string) (*telebot.Bot, error) {
	// Init TG API Bot.
	bot, err := telebot.NewBot(telebot.Settings{
		Token: apiKey,
		Poller: &telebot.LongPoller{
			Timeout: time.Second * 15,
		},
	})
	if err != nil {
		return nil, fmt.Errorf("can not init Telegram Bot: %w", err)
	}

	setBotRoutes(logger, bot, mailDomain)

	return bot, nil
}

func setBotRoutes(logger *zap.Logger, bot *telebot.Bot, mailDomain string) {
	bot.Handle("/start", func(ctx telebot.Context) error {
		if _, err := bot.Send(ctx.Chat(), fmt.Sprintf("Hello! I'm the Email Relay bot.\n"+
			"Send your emails to your on-premises SMTP gateway using Recipient %s@%s\n"+
			"and I will forward them to this chat. Good luck!",
			ctx.SenderOrChat().Recipient(), mailDomain)); err != nil {
			logger.Error("Can not send message",
				zap.String("handler", "/start"),
				zap.Error(err),
				zap.String("to", ctx.SenderOrChat().Recipient()))

			return err
		}

		return nil
	})

	bot.Handle("/help", func(ctx telebot.Context) error {
		if _, err := bot.Send(ctx.Chat(), fmt.Sprintf("Hello! I'm the Email Relay bot.\n"+
			"Send your emails to your on-premises SMTP gateway using Recipient %s@%s\n"+
			"and I will forward them to this chat. Good luck!",
			ctx.SenderOrChat().Recipient(), mailDomain)); err != nil {
			logger.Error("Can not send message",
				zap.String("handler", "/start"),
				zap.Error(err),
				zap.String("to", ctx.SenderOrChat().Recipient()))

			return err
		}

		return nil
	})
}

type botSender struct {
	bot *telebot.Bot
}

func (sender *botSender) sendBot(chatID string, text string, attachments ...interface{}) error {
	// Lookup chat.
	chat, err := sender.bot.ChatByID(chatID)
	if err != nil {
		return fmt.Errorf("can not get TG Chat by ID %q: %w", chatID, err)
	}

	// Send Text.
	m, err := sender.bot.Send(chat, text)
	if err != nil {
		return err
	}

	// Send Attachments.
	for i := range attachments {
		if _, err := sender.bot.Reply(m, attachments[i]); err != nil {
			return err
		}
	}

	return nil
}
