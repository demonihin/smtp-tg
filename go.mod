module gitlab.com/demonihin/smtp-tg

go 1.15

require (
	github.com/alash3al/go-smtpsrv/v3 v3.0.1
	github.com/kardianos/service v1.1.0
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/zap v1.16.0
	gopkg.in/tucnak/telebot.v3 v3.0.0-20201104205009-cde97c26cfda
)
