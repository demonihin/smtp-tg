package main

import "github.com/kardianos/service"

type WriteSyncer struct {
	l service.Logger
}

func NewSyncWriter(l service.Logger) *WriteSyncer {
	return &WriteSyncer{
		l: l,
	}
}

func (ws *WriteSyncer) Write(p []byte) (int, error) {
	if err := ws.l.Info(p); err != nil {
		return 0, err
	}

	return len(p), nil
}

func (ws *WriteSyncer) Sync() error {
	return nil
}
