package main

import (
	"flag"
	"log"

	"github.com/kardianos/service"
	"gitlab.com/demonihin/smtp-tg/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func createService(prg *Program, configPath string) (service.Service, error) {
	// Create Service.
	svcConfig := service.Config{
		Description: "SMTP to Telegram Relay",
		DisplayName: "SMTP-TG",
		Name:        "SMTP-TG",
		Arguments: []string{
			"-config", configPath,
		},
	}

	svc, err := service.New(prg, &svcConfig)
	if err != nil {
		return nil, err
	}

	return svc, nil
}

func createLogger(svc service.Service) (*zap.Logger, error) {
	// Init Logger.
	logger, err := svc.Logger(nil)
	if err != nil {
		return nil, err
	}

	// Create Zap Logger.
	zaplogger, err := zap.NewProduction(zap.WrapCore(func(z zapcore.Core) zapcore.Core {
		zapcore.NewCore(zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()),
			NewSyncWriter(logger),
			z)
		return z
	}))
	if err != nil {
		return nil, err
	}

	return zaplogger, nil
}

func main() {
	// Flags.
	var configPath = flag.String("config", "config.json", "Defines a configuration file path.")
	var serviceControl = flag.String("service", "", "Controls the service status.")

	flag.Parse()

	// Parse Configuration.
	cfg, err := config.Load(*configPath)
	if err != nil {
		log.Fatalln(err)
	}

	var prg = NewProgram(cfg)

	// Service.
	svc, err := createService(prg, *configPath)
	if err != nil {
		log.Fatalln(err)
	}

	// Create Logger.
	logger, err := createLogger(svc)
	if err != nil {
		log.Fatalln(err)
	}

	// Set Logger.
	prg.SetLogger(logger)

	// Control (Install, Remove).
	if len(*serviceControl) != 0 {
		if err := service.Control(svc, *serviceControl); err != nil {
			log.Printf("Valid actions: %q\n", service.ControlAction)
			log.Fatalln(err)
		}

		return
	}

	// Run Service.
	if err = svc.Run(); err != nil {
		logger.Error("Error during service run", zap.Error(err))
	}
}
