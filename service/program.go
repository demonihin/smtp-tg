package main

import (
	"context"

	"github.com/kardianos/service"
	smtptg "gitlab.com/demonihin/smtp-tg"
	"gitlab.com/demonihin/smtp-tg/config"
	"go.uber.org/zap"
)

type Program struct {
	ctx context.Context
	cf  context.CancelFunc

	cfg *config.Configuration

	logger *zap.Logger
}

func NewProgram(cfg *config.Configuration) *Program {
	return &Program{
		cfg: cfg,
	}
}

func (p *Program) SetLogger(l *zap.Logger) {
	p.logger = l
}

func (p *Program) Start(s service.Service) error {
	// Set Logger.
	if p.logger == nil {
		l, err := zap.NewProduction()
		if err != nil {
			return err
		}

		p.logger = l
	}

	// Init Context.
	if p.ctx == nil {
		ctx, cf := context.WithCancel(context.Background())

		p.cf = cf
		p.ctx = ctx
	} else {
		ctx, cf := context.WithCancel(p.ctx)

		p.cf = cf
		p.ctx = ctx
	}

	go p.run()

	return nil
}

func (p *Program) Stop(s service.Service) error {
	// Cancel Context.
	p.cf()

	return nil
}

func (p *Program) run() {
	inst, err := smtptg.NewContext(p.ctx,
		p.logger,
		p.cfg.SMTPBind, p.cfg.BotAPIKey, p.cfg.EmailDomain,
		func(username, password string) error { return nil })
	if err != nil {
		p.logger.Fatal("Can not init smtptg", zap.Error(err))
	}

	if err := inst.ListenAndServe(); err != nil {
		p.logger.Error("Error ListenAndServe", zap.Error(err))
	}
}
