package main

import (
	"context"
	"flag"
	"log"

	smtptg "gitlab.com/demonihin/smtp-tg"
	"gitlab.com/demonihin/smtp-tg/config"
	"go.uber.org/zap"
)

func main() {
	// Init Flags.
	configFile := flag.String("config", "config.json", "Defines a path to configuration file")

	flag.Parse()

	// Load Configuration.
	cfg, err := config.Load(*configFile)
	if err != nil {
		log.Fatalln(err)
	}

	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalln(err)
	}

	inst, err := smtptg.NewContext(context.Background(),
		logger,
		cfg.SMTPBind,
		cfg.BotAPIKey,
		cfg.EmailDomain,
		func(username, password string) error { return nil })
	if err != nil {
		log.Fatalln(err)
	}

	log.Fatalln(inst.ListenAndServe())
}
